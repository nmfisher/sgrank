(defproject com.avinium/sgrank "0.0.1-SNAPSHOT"
  :description "(Modified) implementation of SGRank algorithm"
  :dependencies [
                  [org.clojure/clojure "1.8.0"]
                  [com.avinium/dictionary "0.0.2-SNAPSHOT"]
                  [org.clojure/java.jdbc "0.6.1"]
                  [org.postgresql/postgresql "9.4-1201-jdbc41"]
                  [ubergraph "0.2.2"]
                  [org.clojure/tools.cli "0.3.5"]
                  [org.clojure/tools.logging "0.3.1"]]
  :javac-options ["-target" "1.6" "-source" "1.6" "-Xlint:-options"]
  :jvm-opts ["-Xmx10G" "-server"]
  :aot [com.avinium.sgrank.core]
  :main com.avinium.sgrank.core
  :source-paths ["src/main/clojure"]
  :test-paths ["src/test/clojure"])
