(ns com.avinium.sgrank.sgranker-test
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [com.avinium.sgrank.sgranker :refer :all])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:require [clojure.data :refer :all])
  (:import java.io.File)
)

(deftest test-sort-by-keyword
  (is (= { :baz { :tfIdf 3 } :bar { :tfIdf 2 } :foo { :tfIdf 1} }
          (sort-by-keyword { :foo { :tfIdf 1} :baz { :tfIdf 3 } :bar { :tfIdf 2 }} :tfIdf ))
      "sort a map of ngrams by tf-idf score descending")
  (is (= { :baz { :sWeight 3 } :bar { :sWeight 2 } :foo { :sWeight 1} }
          (sort-by-keyword  { :bar { :sWeight 2 } :baz { :sWeight 3 } :foo { :sWeight 1} } :sWeight))
      "sort a map of ngrams by sWeight descending")
  (is (= 3
         (count (sort-by-keyword { '(:foo) { :tfIdf 1} '(:baz) { :tfIdf 3 } '(:bar) { :tfIdf 2 }} :tfIdf)))
      "if entries have equal tf-idf scores, they are not inadvertently removed"))

(run-tests)
