(ns com.avinium.sgrank.core-test
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [com.avinium.sgrank.core :refer :all])
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:require [clojure.data :refer :all])
  (:import java.io.File)
)

(deftest test-sgrank
  "Integration test - SGRank a block of text"
  (-main
    "But they, I am sure, will join us in saying, as we bid farewell to the dust of these martyr-heroes, that wheresoever throughout the civilized world the accounts of this great warfare are read, and down to the latest period of recorded time, in the glorious annals of our common country, there will be no brighter page than that which relates the Battles of Gettysburg."
    "--corpusPath"  (.getParent (File. (.toURI (io/resource "test/corpus/1.txt"))))))

(deftest test-sgrank-with-stopwords-file
  "Integration test - SGRank a block of text with a stopwords file"
  (-main
    "But they, I am sure, will join us in saying, as we bid farewell to the dust of these martyr-heroes, that wheresoever throughout the civilized world the accounts of this great warfare are read, and down to the latest period of recorded time, in the glorious annals of our common country, there will be no brighter page than that which relates the Battles of Gettysburg."
    "--corpusPath" (.getParent (File. (.toURI (io/resource "test/corpus/1.txt"))))
    "--stopWords" (.toString (io/resource "stopwords.txt"))))

(deftest test-sgrank-lowercase-only
  "Integration test - SGRank a block of text using lowercase only"
  (-main
    "But they, I am sure, will join us in saying, as we bid farewell to the dust of these martyr-heroes, that wheresoever throughout the civilized world the accounts of this great warfare are read, and down to the latest period of recorded time, in the glorious annals of our common country, there will be no brighter page than that which relates the Battles of Gettysburg."
    "--corpusPath" (.getParent (File. (.toURI (io/resource "test/corpus/1.txt"))))
    "--stopWords" (.toString (io/resource "stopwords.txt"))
    "--lc"))

(run-tests)
