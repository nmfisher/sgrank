(ns com.avinium.sgrank.sgranker-test
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [com.avinium.sgrank.sgranker :refer :all])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:require [clojure.data :refer :all])
  (:import java.io.File)
)

(deftest test-options
  (is (= (str #"^(one|two|three)$")
          (str (:stopWordsRegex (sgrank-options 1 {:stopWords (io/resource "test/stopwords.txt") }))))))

(run-tests)
