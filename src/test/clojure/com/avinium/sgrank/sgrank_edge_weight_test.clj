(ns com.avinium.sgrank.sgranker-test
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [com.avinium.sgrank.sgranker :refer :all])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:require [clojure.data :refer :all])
  (:import java.io.File)
)


(deftest test-inside-cooccurrence-window
  (is (= true (inside-cooccurrence-window [100 1000] [200 2000] { :cooccurrenceWindow 101 }))
      "two terms are inside the cooccurrence window of 101 because 200 - 100 = 100; < 101")
  (is (= false (inside-cooccurrence-window [100 1000] [200 2000] { :cooccurrenceWindow 99 }))
      "two terms are outside the cooccurrence window of 101 because 200 - 100 = 100; > 99"))

(deftest test-list-log-cooccurrences
  (is (= true (list-log-cooccurrences [100 1000] [200 2000] { :cooccurrenceWindow 101 }))
      "two terms are inside the cooccurrence window of 101 because 200 - 100 = 100; < 101"))
