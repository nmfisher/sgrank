(ns com.avinium.sgrank.sgranker-test
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [com.avinium.sgrank.sgranker :refer :all])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:require [clojure.data :refer :all])
  (:import java.io.File)
)

(deftest test-tf-idf
  "tf-idf score for a known word"
  (is (< 0 (tf-idf ["leucocyte"] { :tf 1 } (dict/create 10 { ["leucocyte"] { :df 1 :tf 1 } })))))

(deftest test-tf-idf-unknown
  "tf-idf score for an unknown word"
  (is (< 0 (tf-idf ["leucocyte"] { :tf 1 } (dict/create)))))

(deftest test-tf-idf-unknown
  (is (= {} (eliminate-unlikely { ["two" "one"] { :tf 1 :df 1}} {:stopWordsRegex #"one" :minWordLength 0 :frequencyThreshold 0 :discardableCharsRegex #"\%" }))
      "eliminate n-grams containing any stop words")
  (is (= {} (eliminate-unlikely { ["two" "one"] { :tf 1 :df 1}} {:stopWordsRegex #"" :minWordLength 4 :frequencyThreshold 0 :discardableCharsRegex #"\%" }))
      "eliminate n-grams containing words less than the min word length")
  (is (= {} (eliminate-unlikely { ["one"] { :tf 1 :df 1}} {:stopWordsRegex #"two" :minWordLength 0 :frequencyThreshold 2 :discardableCharsRegex #"\%" }))
      "eliminate n-grams not meeting the frequency threshold")
  (is (= {} (eliminate-unlikely { ["one%"] { :tf 1 :df 1}} {:stopWordsRegex #"two" :minWordLength 0 :frequencyThreshold 0 :discardableCharsRegex #"\%" }))
      "eliminate n-grams containing discardable characters"))

(run-tests)
