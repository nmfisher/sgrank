(ns com.avinium.sgrank.sgranker
  (:gen-class)
  (:require [clojure.string :as str])
  (:require [ubergraph.core :as uber])
  (:require [ubergraph.alg :as uberalg])
  (:require [com.avinium.sgrank.document :as document])
  (:require [com.avinium.sgrank.document :as document])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [com.avinium.dictionary.parser :as parser])
)

(load "sgrank_initial")
(load "sgrank_rerank")
(load "sgrank_unlikely")
(load "sgrank_edge_weight")
(load "sgrank_tograph")
(load "sgrank_pagerank")
(load "sgrank_options")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- rank-ngrams
  [ngramMap corpus options]
  (-> (eliminate-unlikely ngramMap options)
      (initial-rank corpus)
      ((fn [s] (do (println "INITIAL") (print s) s)))
      (re-rank options corpus)
      ((fn [s] (do (println "RERANKED") (print s) s)))
       ; eliminative ngrams with negative sWeights
      (->> (filter (fn [[k v]] (> (:sWeight v) 0))))
      ((fn [s] (do (println "NEGATIVE ELIMINATED") (print s) s)))
      (to-graph options)
      ; page-rank the graph
      (page-rank options)
      ; convert the graph to a list of terms and PageRank scores
      ((fn [graph] (map (fn [node] [node (uber/attr graph node :pageRank)]) (uber/nodes graph))))
      (->> (sort #(compare (last %2) (last %1))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn rank
  [inputStream corpus options]
  (println (sgrank-options 1 options))
  ; parse the stream into a dictionary (merging the user provided options with the default SGRank options)
    (let [parsed (parser/parse inputStream (sgrank-options 1 options))
          ngramMap (:ngrams parsed)
          ; take the total frequency of all 1-grams as the length of the document (note this will exclude stop-words, words containing excluded chars, etc)
          docLength (count (filter (fn [[k v]] (= 1 (count k))) ngramMap))]
    ; perform SGRank on the ngrams, using the actual document length to calculate the frequency threshold
    (rank-ngrams ngramMap corpus (sgrank-options docLength options))))
