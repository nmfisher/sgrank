(ns com.avinium.sgrank.db
  (:gen-class)
  (:require [clojure.java.jdbc :as sql])
  (:import com.mchange.v2.c3p0.ComboPooledDataSource))

(def db-spec (atom {}))

(defn pool
  [spec]
  (let [cpds (doto (ComboPooledDataSource.)
               (.setDriverClass (:classname spec))
               (.setJdbcUrl (str "jdbc:" (:subprotocol spec) ":" (:subname spec)))
               (.setUser (:user spec))
               (.setPassword (:password spec))
               ;; expire excess connections after 30 minutes of inactivity:
               (.setMaxIdleTimeExcessConnections (* 30 60))
               ;; expire connections after 3 hours of inactivity:
               (.setMaxIdleTime (* 3 60 60)))]
    {:datasource cpds}))

(def pooled-db (delay (pool (deref db-spec))))

(defn db-connection [] @pooled-db)

(defn init [spec]
  (do
    (swap! db-spec (fn [& args] spec))
    (try
      (sql/db-do-commands (db-connection)
        [(sql/create-table-ddl :documents [[:id :text "not null" "unique"]])])
      (catch Exception e (str e)))
    (try
      (sql/db-do-commands (db-connection)
        [(sql/create-table-ddl :ngrams [[:term :text "not null" "unique"][:tf :int "not null"][:df :int "not null"]])])
      (catch Exception e (str e)))))
