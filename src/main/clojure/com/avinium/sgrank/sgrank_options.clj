(in-ns 'com.avinium.sgrank.sgranker)

(defn- default-frequency-threshold
  [len]
   (if (< len 1500)
       0
       (if (< len 3000)
         2
         3)))

(defn- read-stop-words
  [path]
  (-> (slurp path)
      (str/replace #"\n" "|")
      (->> (str "^("))
      (str ")$")
      (re-pattern)))

(defn sgrank-options
  ([]
    (sgrank-options 1))
  ([docLength]
     (sgrank-options docLength {}))
  ([docLength options]
     (-> options
         (assoc :keepPositions true) ;this is needed for the com.avinium.dictionary library to preserve the position of every ngram parsed
         (assoc :stopWordsRegex
                 (if (:stopWords options)
                     (read-stop-words (:stopWords options))
                     (re-pattern #"^(the|a|an|and|as|at|be|but|by|for|from|in|it|is|of|on|to|up|under|will|with|that|they|this|were|was)$")))
     (->> (merge
        {:splitRegex (re-pattern #"'s|[^\w-&]")
         :discardableCharsRegex (re-pattern #"[^\w'.\(\)]")
         :minThreshold (default-frequency-threshold docLength)
         :nMin 1
         :nMax 6
         :minWordLength 1
         :cooccurrenceWindow 1500
         :pfoCutoff 3000
         :dampingFactor 0.15
         :maxPageRankIterations 10
         :reRankCandidates 100 })))))


