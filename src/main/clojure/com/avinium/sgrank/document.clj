(ns com.avinium.sgrank.document
  (:gen-class)
  (:require [clojure.java.jdbc :as sql])
  (:require [com.avinium.sgrank.db :as db])
  (:import com.mchange.v2.c3p0.ComboPooledDataSource))

(defn save [id]
  (nth
    (sql/insert!
      (db/db-connection)
        :documents {
          :id id }) 0))

(defn exists? [id]
  (sql/query
    (db/db-connection)
      ["SELECT count(*) FROM documents WHERE id=?" id]))

(defn corpus-size []
  (let
    [corpusSize
     (get (nth
        (sql/query
          (db/db-connection)
            ["SELECT count(*) FROM documents"])
        0)
      :count)]
    corpusSize))
