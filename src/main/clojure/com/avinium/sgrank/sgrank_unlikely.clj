(in-ns 'com.avinium.sgrank.sgranker)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn contains-special-chars?
  [word regex]
  (not= nil (re-find regex word)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn some-contains-special-chars?
  [term regex]
  (not= nil
    (some #(contains-special-chars? % regex) term)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; discard ngrams containing only stop words , words not noun adjective or verb |(the)|(and)|(at)|(of)|(to) - modified from original SGRANK
;;
(defn- is-stop-word?
  [word regex]
  (not= nil (re-find regex word)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- starts-or-ends-with-stop-word?
  [term regex]
  (or
    (is-stop-word? (last term) regex)
    (is-stop-word? (first term) regex)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- contains-stop-words?
  [term regex]
  (some #(is-stop-word? % regex) term))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- only-contains-stop-words?
  [term regex]
  (every? #(is-stop-word? % regex) term))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- under-min-word-length?
  [term length]
  (some #(< (count %) length) term))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- discardable?
  [[term mapping] options]
    (or
      (< (:tf mapping) (:minThreshold options))
      (under-min-word-length? term (:minWordLength options))
      (contains-stop-words? term (:stopWordsRegex options))
      (starts-or-ends-with-stop-word? term (:stopWordsRegex options))
      (only-contains-stop-words? term (:stopWordsRegex options))
      (some-contains-special-chars? term (:discardableCharsRegex options))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; discard ngrams first on stop word rules, then frequency threshold
;; rules for the given document length
(defn eliminate-unlikely
  [ngramMap options]
  ; remove any ngrams that are discardable (contain stopwords etc) or that fall below the frequency threshold
  (reduce (fn [accum [k v]] (if (discardable? [k v] options) (dissoc accum k) accum)) ngramMap ngramMap))

