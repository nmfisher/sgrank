(in-ns 'com.avinium.sgrank.sgranker)

(defn idf
  [df corpusSize]
  ; idf = log(N/df)
  (if (> df 0)
      (Math/log (/ corpusSize df))
      ; unless df is zero, meaning the given term does not appear in any other documents
      ; if we assume our corpus is comprehensive, a word with df=0 is important
      (if (= 0 corpusSize)
          1
          (Math/log corpusSize))))

(defn adjusted-idf
  [term corpus]
  (if (> (count term) 1)
    1
    (idf (dict/df corpus term) (:corpusSize corpus))))

(defn tf-idf
  [term mapping corpus]
  (->> (adjusted-idf term corpus)
       (* (:tf mapping))))
