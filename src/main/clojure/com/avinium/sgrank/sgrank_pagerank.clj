(in-ns 'com.avinium.sgrank.sgranker)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sum the weights of every edge attached to a node
;;
(defn sum-edge-weights
  [node graph]
  (->> (uber/out-edges graph node)
       (reduce (fn [accum edge] (+ accum (uber/attr graph edge :weight)) 0))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; normalizes a specific edge in a graph by dividing the edge weight by the sum of weight of all outgoing edges for that node
;;
(defn normalize-edge
  [graph edge]
  (let [  srcNode (uber/src edge)
          edgeWeight (uber/attr graph edge :weight)
          weightSum (sum-edge-weights srcNode graph)
         normalizedWeight (/ edgeWeight weightSum)]
    (uber/set-attrs graph edge { :weight normalizedWeight  })))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; normalize every out-edge for a node
;;
(defn normalize-node
  [graph node]
  (->> (uber/out-edges graph node)
       (reduce #(normalize-edge %1 %2))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn sum-normalized-in-edge-weights
  [i graph]
  (reduce +
    (for
      [iEdge (uber/in-edges graph i)]
      (let [j (uber/src iEdge)
           jiWeight (uber/attr graph iEdge :weight)
           jkEdges (uber/out-edges graph j)
            sumJkEdgeWeights (reduce + (map #(uber/attr graph % :weight) jkEdges))]
        (/ jiWeight sumJkEdgeWeights)))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn page-rank-node
  [graph node options]
  ; grab the last page rank, or 1 if not available
  (->> (or (uber/attr graph node :pageRank) 1)
       ; add (1 - damping factor) to
       ; the damping factor
       ; multiplied by the sum, for every incoming edge, of the edge-weighted PageRank of this node
           ; divided by the sum of the weights for all outgoing edges
       ; this simulates a random walker on the graph - see algorithm paper for more details
       (+
         (- 1 (:dampingFactor options))
         (* (:dampingFactor options)
            (sum-normalized-in-edge-weights node graph)))
       ; set this as the new page rank
      (assoc {} :pageRank)
      (uber/set-attrs graph node)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; a page rank has stabilized if it is within 0.1% of its previous page rank
;;
(defn stable?
  [currentRank prevRank]
    (and (not= nil prevRank) (< (* 100 (/ (- prevRank currentRank) prevRank)) 0.1)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; apply page rank to a node until its rank has stabilized
;;
(defn iterative-page-rank-node
  [graph node i options]
    (let [oldRank (uber/attr graph node :pageRank)
         newGraph (page-rank-node graph node)
         newRank (uber/attr newGraph node :pageRank)]
    (if (or
            (< i (get options :maxPageRankIterations))
            (not stable? oldRank newRank))
        (recur newGraph node (inc i) options)
        newGraph)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; rank every node in the graph using an equivalent PageRank algorithm
;;
(defn page-rank
  [graph options]
  (reduce #(page-rank-node %1 %2 options) graph (uber/nodes graph)))




