(in-ns 'com.avinium.sgrank.sgranker)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn is-subsumed-in [child parent]
  (if (> (count child) (count parent))
    false
    (every? #(contains? (set parent) %) (set child)))) ; lazy way to check for subsumption as this does not account for word order, but should be good enough

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- subsumption-count
  [term ngramMap]
  (->> ngramMap
       (filter (fn [[k v]] (and (not= k term) (is-subsumed-in term k))))
       (map #(:tf (last %)))
       (reduce +)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- pfo
  [term mapping options]
  (Math/log (/ (:pfoCutoff options) (inc (last (:positions mapping [0]))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn- TL
  [term]
  (count term))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn statistical-weight
  [term mapping ngramMap options corpus]
  (let [subsumptionCount (subsumption-count term ngramMap)
        doc_idf (adjusted-idf term corpus)]
    (do
      (print "subsumption count for" term subsumptionCount)
      (print "doc_idf for" term doc_idf)
      (print "tf for" term (:tf mapping))
      (print "pfo for" term (pfo term mapping options))
      (print "TL for" term (TL term))
    (*
      (- (:tf mapping) subsumptionCount)
      doc_idf
      (pfo term mapping options)
      (TL term)))))
