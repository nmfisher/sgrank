(in-ns 'com.avinium.sgrank.sgranker)

(defn has-more? [x]
  (> (count x) 1))

(defn inside-cooccurrence-window
  ([positions1 positions2 options]
   (loop [p1 positions1]
     (if
        (loop [p2 positions2]
          (if (inside-cooccurrence-window (- (first p1) (first p2)) options)
              true
              (if (has-more? p2)
                  (recur (rest p2))
                  false)))
        true
       (if (has-more? p1)
           (recur (rest p1))
           false))))
  ([posDiff options]
    (and (not= 0 posDiff)
         (< posDiff (:cooccurrenceWindow options))
          (> posDiff (- (:cooccurrenceWindow options))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn log-pos-diff
  [pos1 pos2 options]
  (let [posDiff (Math/abs (- pos1 pos2))]
    (if (inside-cooccurrence-window posDiff options)
      (Math/log (/ (:cooccurrenceWindow options) (Math/abs posDiff))))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn list-log-cooccurrences
  [positions1 positions2 options]
  (->> positions1
      (mapcat (fn [p1] (map (fn [p2] (log-pos-diff p1 p2 options)) positions2)))
      (filter identity)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn decayed-weight
  [positions1 positions2 options]
  (let
    [listLogCooccurrences (list-log-cooccurrences positions1 positions2 options)
     numCooccurrences (count listLogCooccurrences)]
    (if (= 0 numCooccurrences)
      0
      (/ (reduce + listLogCooccurrences) numCooccurrences))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn edge-weight
  [mapping1 mapping2 options]
  (* (decayed-weight (:positions mapping1) (:positions mapping2) options) (:sWeight mapping1) (:sWeight mapping2)))
