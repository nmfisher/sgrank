(ns com.avinium.sgrank.core
  (:gen-class)
  (:require [com.avinium.sgrank.sgranker :as sgranker])
  (:require [com.avinium.dictionary.core :as dict-core])
  (:require [com.avinium.dictionary.dict :as dict])
  (:require [com.avinium.dictionary.parser :as parser])
  (:require [clojure.string :as str])
  (:require [clojure.java.io :as io])
  (:require [ubergraph.core :as uber])
  (:require [ubergraph.alg :as uberalg])
  (:require [clojure.tools.cli :refer [parse-opts]])
  (:require [clojure.tools.logging :as log]))

(def cli-options
    (into
      dict-core/cli-options
      [[nil "--file" "if true, the passed argument will be treated as a filepath and the contents of the target path will be parsed"]
       [nil "--corpusPath PATH" "the path to a directory containing all documents that will form the reference corpus"]
       [nil "--stopWords PATH" "the path to a text file containing stop words that will be excluded"]
       [nil "--minWordLength LENGTH" "the minimum length for every single part of an n-gram"
        :parse-fn #(Integer/parseInt %)
        :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]
        :default 1]
       [nil "--cooccurrenceWindow LENGTH" "Coocurrence window"
        :parse-fn #(Integer/parseInt %)
        :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]
        :default 1500]
       [nil "--pfoCutoff CUTOFF" "PFO cutoff"
        :parse-fn #(Integer/parseInt %)
        :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]
        :default 3000]
       [nil "--dampingFactor DF" "Damping factor"
        :parse-fn #(Float/parseFloat %)
        :validate [#(< 0 % 1) "Must be a number between 0 and 1"]
        :default 0.15]
       [nil "--maxPageRankIterations I" "Max number of PageRank iterations"
        :parse-fn #(Integer/parseInt %)
        :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]
        :default 10]
       [nil "--reRankCandidates R" "Number of n-gram candidates surviving first cut"
        :parse-fn #(Integer/parseInt %)
        :validate [#(< 0 % 0x20000) "Must be a number between 0 and 131072"]
        :default 100]]))

(defn- usage [options-summary]
  (->> ["Parse a document into n-grams ranked according to SGRank"
        ""
        "Usage: java -jar genix-standalone.jar [targetfile]"
        ""
        "Options:"
        options-summary]
       (str/join \newline)))

(defn- error-msg [errors]
  (str "The following errors occurred while parsing your command:\n\n"
       (str/join \newline errors)))

(defn- exit [status msg]
  (println msg)
  (System/exit status))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn start-rank [stream corpus options]
   (let [ranked (sgranker/rank stream corpus options)]
     (if (= 0 (count ranked))
         (println "No rankable terms found...")
         (doseq [term ranked]
           (println
             "Word : " (nth term 0)
             "Rank : " (nth term 1))))
     ranked))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn -main [& args]
  (let [{:keys [options arguments errors summary]} (parse-opts args cli-options)]
    (cond
        (:help options) (exit 0 (usage summary))
        (= 0 (count arguments)) (exit 1 (usage summary))
        errors (exit 1 (error-msg errors)))
    (let [target (if (:file options) (io/reader (nth arguments 0)) (java.io.ByteArrayInputStream. (.getBytes (nth arguments 0))))

          corpus
            (if (:corpusPath options)
                (parser/recurse (:corpusPath options) options)
                (do (dict-core/init-db (dict-core/parse-db-spec options))
                    (dict/load-dict)))]
          (start-rank target corpus options))))
