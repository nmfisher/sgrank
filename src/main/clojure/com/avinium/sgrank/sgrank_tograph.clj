(in-ns 'com.avinium.sgrank.sgranker)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn pairable?
  [[term1 mapping1] [term2 mapping2] options]
  (and (not= term1 term2)
       (inside-cooccurrence-window (:positions mapping1) (:positions mapping2) options)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ; return a list of pairs for the given ngram and every other ngram that meets the predicate
(defn collect-pairs-for-ngram
  [[term1 mapping1] ngramMap options]
  (filter identity
    (for [[term2 mapping2] ngramMap] (if (pairable? [term1 mapping1] [term2 mapping2] options) [[term1 mapping1] [term2 mapping2]]))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  split a list of ngrams into a list of ngram pairs
;;
(defn collect-pairs
  [ngramMap options]
  (->> ngramMap
        ; pair every ngram with every other ngram that meets the specifed condition
       (mapcat (fn [[k v]] (collect-pairs-for-ngram [k v] ngramMap options)))
        ; remove any null pairings
       (filter identity)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  return a vector of unique ngram pairs with their associated edge weight
;;
(defn weigh-pair
  [[[term1 mapping1][term2 mapping2]] options]
  [ term1 term2 (edge-weight mapping1 mapping2 options)])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;  return a vector of unique ngram pairs with their associated edge weight
;;
(defn pair-and-weigh-edges
  [ngramMap options]
  ; for every ngram, produce a list of possible ngram pairs
  (->> (collect-pairs ngramMap options)
      ; calculate the edge weight of each pair
       (map #(weigh-pair % options))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ; add each term to a graph
;;
(defn to-graph
  [ngramMap options]
  (->> (pair-and-weigh-edges ngramMap options)
       (apply uber/digraph)))
