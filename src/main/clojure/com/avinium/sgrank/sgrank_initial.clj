(in-ns 'com.avinium.sgrank.sgranker)

(load "sgrank_tf_idf")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; apply a boost to capitalized terms - custom addition not in original SGRank algorithm
;;
(defn- boost [term mapping]
  (if  (and (Character/isUpperCase (first (first term))) (Character/isUpperCase (first (last term))))
        (* 2 (:tfIdf mapping))
        (:tfIdf mapping)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;
;;
(defn initial-rank
  [ngramMap corpus]
   (->>
    ;calculate the tf-idf score for each ngram
     (reduce (fn [accum [term mapping]] (assoc accum term (merge mapping { :tfIdf (tf-idf term mapping corpus) }))) ngramMap ngramMap)))
        ;(reduce (fn [accum [term mapping]] (assoc accum term (merge mapping { :tfIdf (boost term mapping) }))) {})))
