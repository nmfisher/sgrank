(in-ns 'com.avinium.sgrank.sgranker)

(load "sgrank_statistical_weight")


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; sort an ngram map by the value associated with the specified keyword
;; e.g. tfIdf, sWeight, etc
;; sort is descending
(defn sort-by-keyword
  [ngramMap kw]
  (into
    (sorted-map-by (fn [k1 k2] (compare [(get (get ngramMap k2) kw) k2] [(get (get ngramMap k1) kw) k1])))
    ngramMap))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; calcualte the statistical weight for every ngram in the ngramMap
;;
;;
(defn calculate-s-weight
  [ngramMap options corpus]
  (reduce (fn [m [k v]] (assoc-in m [k :sWeight] (statistical-weight k v m options corpus))) ngramMap ngramMap))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; re-ranks each ngram by its statistical weight
;;
(defn re-rank
  [ngramMap options corpus]
    ; sort the list by tfIdf score
    (-> ngramMap
        (sort-by-keyword :tfIdf)
         ; take the top T candidates
        (->> (take-last (:reRankCandidates options))
             (into {}))
         ; calculate the s-weight for every ngram
         (calculate-s-weight options corpus)
         (sort-by-keyword :sWeight)))


